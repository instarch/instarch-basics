#!/bin/bash

for pkg in $(find . -name "PKGBUILD"); do
  cd $(dirname ${pkg})

  makepkg -sf

  cd -
done

# vim:set et sw=2 ts=2:
