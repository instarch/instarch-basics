#!/bin/bash

SCRIPT=$(readlink -f $0 )
SCRIPTPATH=$(dirname ${SCRIPT} )

sudo apt-get update && sudo apt-get install tmux vim vim-scripts zsh

# VIM
sudo ln -sf ${SCRIPTPATH}/config/vim/vimrc /etc/vim/vimrc.local

# ZSH
sudo ln -sf ${SCRIPTPATH}/config/zsh/zshrc /etc/zsh/zshrc

# TMUX
sudo ln -sf ${SCRIPTPATH}/config/tmux/tmux.conf /etc/tmux.conf
sudo ln -sf ${SCRIPTPATH}/config/tmux/tmux /etc/tmux

# Git
sudo ln -sf ${SCRIPTPATH}/config/git/gitconfig /etc/
sudo ln -sf ${SCRIPTPATH}/config/git/gitignore /etc/

# VI keybindings
grep -q "set editing-mode vi" /etc/inputrc
if [[ $? -eq 1 ]]; then
  sudo sed -i '1iset editing-mode vi\n' /etc/inputrc
fi

# vim:et ts=2 sw=2 ai:
