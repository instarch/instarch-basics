#!/bin/bash

DIR=/etc/i3statusgen
HEAD=${DIR}/i3status.head
TAIL=${DIR}/i3status.tail

OUT=/dev/shm/i3statusgen.conf
WRITE=0
DEVNAME=0
ORDER=""

while true; do
  case "$1" in
    -w)
      WRITE=1
      shift
      ;;
    -d)
      DEVNAME=1
      shift
      ;;
    *)
      break
      ;;
  esac
done

# start the configuration file
if [ -e ${HEAD} ]; then
  cat ${HEAD} > ${OUT}
else
  echo "" > ${OUT}
fi

# begin generated content
echo "# GENERATED WITH I3STATUSGEN" >> ${OUT}

# disk usage
IFS=$'\n'
for disk in $(lsblk -o TYPE,MOUNTPOINT | awk '/\// {if ($1 !~ /(rom|loop)/) print substr($0, index($0, $2))}' | sort | uniq ); do
  ORDER="${ORDER}\norder += \"disk ${disk}\""
  if [ $DEVNAME -eq 1 ]; then
    dev=$(lsblk -l | grep "\<$(mountpoint -d "${disk}" )\>" | cut -d" " -f1 | head -1 )
  else
    dev=${disk}
  fi

  cat >> ${OUT} <<EOT
disk "${disk}" {
    format = "${dev}: %free"
}
EOT
done

# network information
ifname=$(ip route | awk '/^default/ {print $5}' )
if [[ "${ifname}" != "" ]]; then
  is_wireless=$(iwconfig 2>&1 | grep "^${ifname}.*ESSID" )
  if [[ "${is_wireless}" == "0" ]]; then
    ORDER="${ORDER}\norder += \"wireless ${ifname}\""
    cat >> ${OUT} <<EOT
wireless ${ifname} {
  format_up = "${ifname}: (%quality at %essid, %bitrate) %ip"
  format_down = "${ifname}: down"
}
EOT
  else
    ORDER="${ORDER}\norder += \"ethernet ${ifname}\""
    cat >> ${OUT} <<EOT
ethernet ${ifname} {
  # if you user %speed, i3status requires root privileges
  format_up = "${ifname}: %ip"
  format_down = "${ifname}: down"
}
EOT
  fi
fi

# battery indicator
for battery in $(find /sys/class/power_supply -iname "BAT*" ); do
  bid=${battery/*BAT/}
  ORDER="${ORDER}\norder += \"battery ${bid}\""
  cat >> ${OUT} <<EOT
battery ${bid} {
  format = "%status %percentage %remaining %emptytime"
  path = "/sys/class/power_supply/BAT%d/uevent"
  low_threshold = 10
}
EOT
done

echo -e "${ORDER}" >> ${OUT}

# include a tail if one exists
[ -e ${TAIL} ] && cat ${TAIL} >> ${OUT}

if [ ${WRITE} -eq 1 ]; then
  cp /etc/i3status.conf{,.genorig}
  mv ${OUT} /etc/i3status.conf
else
  cat ${OUT}
fi

# vim:ts=2 sw=2 et ai syntax=sh:
