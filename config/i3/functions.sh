#!/bin/bash

exec_once() {
  local proc_name=${2:-$1}
  $(pgrep -f "${proc_name}" > /dev/null)
  local res=$?
  if [[ "${res}" == "1" ]]; then
    exec $1 &
  fi
}

exec_if_exists() {
  [[ -f "$1" ]] && exec $2 $1 &
}
