#!/bin/bash

source /etc/i3/functions.sh

if [[ -f ~/.config/i3/autostart.sh ]]; then
  bash ~/.config/i3/autostart.sh
else
  exec_once "xfce4-clipman"
  exec_once "volumeicon"
  exec_once "systemctl --user start gpg-agent" "gpg-agent"
  exec_once "pulseaudio"

  exec numlockx on &
  systemctl --user start gpg-agent

  exec_if_exists ~/.screenlayout/default.sh
  exec_if_exists ~/.fehbg sh
fi

if $(grep -qi VBOX /proc/scsi/scsi); then
  /usr/bin/VBoxClient-all 2>&1 /dev/null
fi

# vim:et sw=2 ts=2:
