set expandtab shiftwidth=4 tabstop=4 list hidden nowritebackup nobackup
set spelllang=en_us spellsuggest=5 ruler textwidth=0 hlsearch wrap cursorline
set nostartofline
set relativenumber number
set incsearch smartindent paste ignorecase
set wildcharm=<Tab> wildmenu wildmode=full
setglobal autoindent
set listchars=precedes:<,extends:>,trail:·,tab:>·
set mouse=a

syntax on
filetype plugin on
set omnifunc=syntaxcomplete#Complete

" Customize the status line
set statusline=%F%m%r%h%w\ [%{&ff}][%Y][A=\%03.3b][H=\%02.2B][POS=%04l,%04v][%p%%][LEN=%L]
set laststatus=2

colorscheme jellybeans

" GVIM-specifics
if has("gui_running")
  if has("gui_gtk2")
    set guifont=Andale\ Mono\ 9
  endif

  set spell
  set nomousehide

  set guioptions=-T
  set guioptions=-t

  " Automatically save the state of files when closed
  autocmd BufWinLeave *.* silent mkview

  " Automatically restore the state of files when opened
  autocmd BufWinEnter *.* silent loadview
else
  set t_Co=256
endif

" Hit escape 3 times to remove highlighting
nnoremap <Esc><Esc><Esc> :noh<CR>

" Pull up NERD Tree and the Tag List plugins
nnoremap <silent> <F6> :NERDTreeToggle<CR>:set norelativenumber nolist<CR>
nnoremap <silent> <F7> :TagbarToggle<CR>
nnoremap <silent> <F8> :TagbarOpenAutoClose<CR>

" Refresh all open buffers
nnoremap <silent> <F5> :set noconfirm<CR>:bufdo e!<CR>:set confirm<CR>

map <F1> <Leader><Leader>w
map <S-F1> <Leader><Leader>W
map <F2> <Leader><Leader>ge
map <S-F2> <Leader><Leader>gE
map <F3> <Leader><Leader>t
map <S-F3> <Leader><Leader>t

" Force writing a file using sudo
command! W :execute ':silent w !sudo tee % > /dev/null' | :edit!

" Replace stuff with line number :%s/\v^\[ ?\d+]/\=line(".")/

" Trim all trailing whitespace before saving
autocmd BufWritePre * :%s/[ \t\r]\+$//e

" Unix line endings FTW
autocmd BufWritePre * :set ff=unix

" Filetype settings {{{
au BufEnter,BufRead,BufNewFile * setlocal relativenumber list
au BufEnter,BufRead,BufNewFile *.inc,*.phtml,*.php setlocal shiftwidth=2 tabstop=2 syntax=php nolist
au BufEnter,BufRead,BufNewFile *.htm,*.html setlocal shiftwidth=2 tabstop=2 syntax=html nolist
au BufEnter,BufRead,BufNewFile *.xml,*.rml setlocal shiftwidth=2 tabstop=2 syntax=xml nolist
au BufEnter,BufRead,BufNewFile *.css setlocal shiftwidth=2 tabstop=2 syntax=css nolist
au BufEnter,BufRead,BufNewFile *.js setlocal shiftwidth=4 tabstop=4 syntax=javascript nolist
au BufEnter,BufRead,BufNewFile *.pde,*.ino setlocal shiftwidth=2 tabstop=2 syntax=c nolist ft=arduino
au BufEnter,BufRead,BufNewFile *.coffee setlocal shiftwidth=2 tabstop=2 syntax=coffee nolist
au BufEnter,BufRead,BufNewFile *.rb setlocal shiftwidth=2 tabstop=2 syntax=ruby nolist
au BufEnter,BufRead,BufNewFile *.hs setlocal shiftwidth=2 tabstop=2 syntax=haskell nolist
au BufEnter,BufRead,BufNewFile *.py setlocal list relativenumber autoindent
au BufEnter,BufRead,BufNewFile *.go setlocal noexpandtab autoindent textwidth=119
au BufEnter,BufRead,BufNewFile *.tf,*.tfvars,*.hcl,*.nomad setlocal ft=hcl expandtab autoindent shiftwidth=2 tabstop=2

au FileType yaml setlocal shiftwidth=2 tabstop=2 autoindent

" highlight HTML embedded in Python files
au FileType python runtime! syntax/python.vim
au FileType python unlet b:current_syntax
au FileType python syntax include @html syntax/html.vim
au FileType python syntax region pythonCode start='"""\n<html>' keepend end='</html>\n"""' contains=@html
" }}}

"autocmd FocusLost *.inc,*.php,*.html,*.js,*.css :w
"au BufWritePost *.coffee silent CoffeeMake! | cwindow

" Highlight trailing spaces
autocmd ColorScheme * highlight ExtraWhitespace ctermbg=darkgreen guibg=darkgreen
highlight ExtraWhitespace ctermbg=darkgreen guibg=darkgreen
match ExtraWhitespace /\s\+\%#\@<!$/

" Highlight diffs
highlight DiffAdded guibg=darkgreen guifg=black
highlight DiffRemoved guibg=darkred guifg=white

" Toggle things that make it more difficult to copy/paste code samples
map <F10> :set invrelativenumber<CR>:set invlist<CR>

" Find all occurrences of the word under the cursor and replace it with the
" specified value
nnoremap <Leader>s :%s/\<<C-r><C-w>\>/
nnoremap Zs /^\(.*\<<C-r><C-w>\>.*\)$<CR>

" Execute the selected expression in Python
vmap <Leader>I "xdmpiprint <Esc>pme`pv`e:!python -<CR>
vmap <Leader>i :!python -<CR>
nnoremap gp `[v`]

" For easier Django blocks
nnoremap <Leader>b i{% block  %}{% endblock %}<Esc>3F i
nnoremap <Leader>B i{% block  %}<CR>{% endblock %}<Esc><Up>$2hi
ab bl <Esc>bi{% block <Esc>ea %}{% endblock %}<Esc>h%i

" Replace pretty quotes with standard ASCII quotes
nnoremap <Leader>" :%s/\v(“\|”)/"/g<CR>
nnoremap <Leader>' :%s/’/'/g<CR>

" For code folding
nnoremap <Leader>x :set fdm=indent foldminlines=10<CR>
nnoremap <Leader>z zA

" Handy for adding an IPython interactive shell for debugging
nnoremap <Leader>d iimport ipdb; ipdb.set_trace()<Esc>

nnoremap tc :tabclose!<CR><CR><CR>

" Space before and after (for math like a+b/c)
nnoremap <Leader>ba i <Esc>la <Esc>

" For RST
vmap <C-h>1 yyPVr=jpVr=o<CR>
vmap <C-h>2 yypVr=o<CR>
vmap <C-h>3 yypVr-o<CR>
vmap <C-h>4 yypVr+o<CR>
vmap <C-h>5 yypVr^o<CR>
vmap <C-h>6 yypVr*o<CR>
vmap <C-b> bi**<Esc>ea**
vmap <C-e> bi*<Esc>ea*
vmap <C-l> i`<Esc>ea <>`_<Esc>hhi

nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" Disable unicode arrows in NERDTree
let g:NERDTreeDirArrows=0
let g:NERDTreeIgnore = ['\.pyc$', '__pycache__', '\.egg-info$', '\.pregen']

" For the mini buffer window
let g:miniBufExplMapWindowNavVim = 1
let g:miniBufExplMapWindowNavArrows = 1
let g:miniBufExplMapCTabSwitchBufs = 1
let g:miniBufExplModSelTarget = 1

" For Tag List plugin
let Tlist_Ctags_Cmd = '/usr/local/bin/ctags'
let Tlist_Use_Right_Window = 1
let Tlist_Inc_Windwidth = 70
let Tlist_Exit_OnlyWindow = 0
nnoremap <Leader>S :TlistSync<CR>

" Syntastic
let g:syntastic_check_on_open=1
let g:syntastic_python_checkers=['flake8', 'pyflakes']

" Flake8 config
let g:flake8_builtins="_"
let g:flake8_max_line_length=99

" Center search results vertically on the screen
nnoremap <silent> n nzz
nnoremap <silent> N Nzz

" Find in files {{{
function! s:FindCurrentWord()
  " Find the word under the cursor in all files (recursively) in the cwd
  normal "wyiw
  call s:FindWordInFS('\<'.@w.'\>')
endfunction

function! s:FindNeedle()
  " Find whatever is in the search buffer in all files (recursively in the
  " current working directory
  call s:FindWordInFS(@/)
endfunction

function! s:FindWordInFS(word)
  " Searches for all instances of a:word in all text files in the current
  " working directory (recursively).  Matches are shown with some context
  " in a new scratch buffer

  tabnew
  setlocal buftype=nofile bufhidden=wipe nobuflisted noswapfile nowrap
  setlocal modifiable
  execute 'silent r!egrep -riInC2 "'.a:word.'" *'
  normal ggdd

  nnoremap <buffer> <Esc> <C-w>c
  nnoremap <buffer> <Return> ^<C-w>gF

  let b:qf_list = []
  python << EOF
import vim
import re

magic = re.compile(r'^([^:-]+):(\d+):(.*)$')

for line in vim.current.buffer:
  matches = magic.match(line)
  if matches:
    vim.command('let l:qf_item = {}')
    vim.command('let l:qf_item.filename = "%s"' % matches.group(1))
    vim.command('let l:qf_item.lnum = %s' % matches.group(2))
    vim.command("let l:qf_item.text = '%s'" % matches.group(3).replace("'", "''"))
    vim.command('call add(b:qf_list, l:qf_item)')
EOF
  call setqflist(b:qf_list, 'r')

  highlight Needle ctermbg=red guibg=red
  exec "match Needle /".a:word."/"
  set nomodifiable
endfunction

command! FindCurrentWord call s:FindCurrentWord()
command! FindNeedle call s:FindNeedle()
nnoremap <Leader>f :FindNeedle<CR>
nnoremap <Leader>g :FindCurrentWord<CR>

" Next/Previous matches in files
nnoremap <silent> <F12> :cn<CR>zz
nnoremap <silent> <F11> :cp<CR>zz
nnoremap <Leader>. :cn<CR>zz
nnoremap <Leader>, :cp<CR>zz

" }}}

" Not sure why this is here {{{
function! BufSel(pattern)
  let bufcount = bufnr("$")
  let currbufnr = 1
  let nummatches = 0
  let firstmatchingbufnr = 0
  while currbufnr <= bufcount
    if(bufexists(currbufnr))
      let currbufname = bufname(currbufnr)
      if(match(currbufname, a:pattern) > -1)
        echo currbufnr . ": ". bufname(currbufnr)
        let nummatches += 1
        let firstmatchingbufnr = currbufnr
      endif
    endif
    let currbufnr = currbufnr + 1
  endwhile
  if(nummatches == 1)
    execute ":buffer ". firstmatchingbufnr
  elseif(nummatches > 1)
    let desiredbufnr = input("Enter buffer number: ")
    if(strlen(desiredbufnr) != 0)
      execute ":buffer ". desiredbufnr
    endif
  else
    echo "No matching buffers"
  endif
endfunction

"Bind the BufSel() function to a user-command
command! -nargs=1 Bs :call BufSel("<args>")
nnoremap <F14> :Bs<Space>

" }}}

" ReSTRUCTUREDTEXT {{{
command! -nargs=+ Shell call s:RunShellCommand(<q-args>)
function! s:RunShellCommand(cmdline)
  " Yank the contents of the current buffer into register z
  normal! %y z

  " Create a new window
  botright new
  setlocal buftype=nofile bufhidden=wipe nobuflisted noswapfile nowrap

  " Paste the contents of the buffer into this new buffer
  normal! ggVG"zP

  " write to a temporary file
  let l:tempfile = tempname()
  execute 'write '.fnameescape(l:tempfile)
  execute 'silent $read !'.a:cmdline.' '.fnameescape(l:tempfile)

  " Hit escape to close this window
  nnoremap <buffer> <Esc> <C-w>c

  setlocal nomodifiable
  1
endfunction

command! ReStructuredText call s:ReStructuredText()
function! s:ReStructuredText()
  call s:RunShellCommand('rst2html.py')

  " Allow modifications
  setlocal modifiable

  " Go to the top of the file
  normal! ggV

  " Clean up the boilerplate HTML
  if search("<body", 'W') > 0
    normal! dGdkgg
  endif

  " Disallow modifications
  setlocal nomodifiable
endfunction

"nnoremap <F2> :ReStructuredText<CR>
" }}}

" TABULARIZE {{{

if exists(':Tabularize')
  nmap <Leader>a= :Tabularize /=<CR>
  vmap <Leader>a= :Tabularize /=<CR>
  nmap <Leader>a: :Tabularize /:\zs<CR>
  vmap <Leader>a: :Tabularize /:\zs<CR>
  nmap <Leader>at :Tabularize /\|<CR>
  vmap <Leader>at :Tabularize /\|<CR>
endif

inoremap <silent> <Bar>   <Bar><Esc>:call <SID>align()<CR>a

function! s:align()
  let p = '^\s*|\s.*\s|\s*$'
  if exists(':Tabularize') && getline('.') =~# '^\s*|' && (getline(line('.')-1) =~# p || getline(line('.')+1) =~# p)
  let column = strlen(substitute(getline('.')[0:col('.')],'[^|]','','g'))
  let position = strlen(matchstr(getline('.')[0:col('.')],'.*|\s*\zs.*'))
  Tabularize/|/l1
  normal! 0
  call search(repeat('[^|]*|',column).'\s\{-\}'.repeat('.',position),'ce',line('.'))
  endif
endfunction
" }}}

" GO {{{
let g:syntastic_go_checkers = ['go', 'golint', 'govet', 'errcheck']
let g:syntastic_mode_map = { 'mode': 'active', 'passive_filetypes': ['go'] }
let g:go_list_type = "quickfix"

"au BufWritePost *.go GoErrCheck
nnoremap <silent> <F9> :GoErrCheck<CR>
" }}}

call plug#begin('~/.vim/plugged')
  Plug 'morhetz/gruvbox'
  Plug 'mileszs/ack.vim'
  Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
  Plug 'fatih/vim-go', { 'tag': '*' }
  Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
  Plug 'nsf/gocode', { 'rtp': 'vim' }
  Plug 'godlygeek/tabular'
  Plug 'regedarek/ZoomWin'
  Plug 'w0ng/vim-hybrid'
  Plug 'nvie/vim-flake8'
call plug#end()

nnoremap <Leader>e :FZF --inline-info<CR>

if executable('ag')
	let g:ackprg = 'ag --nogroup --nocolor --column'
endif

" See: https://github.com/vim-syntastic/syntastic/issues/204
let g:syntastic_python_flake8_args='--ignore=E501'
" let g:syntastic_python_checker_args='--ignore=E501'

" vim:et sw=2 ts=2 ai fdm=marker:
