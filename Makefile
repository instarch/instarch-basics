build :
	./makeall.sh
	make clean

install : build
	find . -name "*.tar.xz" | xargs yaourt -U --noconfirm

clean :
	find . -name "src" -or -name "pkg" -type d | xargs rm -rf
